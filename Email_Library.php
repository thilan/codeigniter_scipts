<?php

/**
  | -------------------------------------------------------------------------
  | Customisable Email Library for in house projects
  | -------------------------------------------------------------------------
  | This library lets you to create and send the most commonly used Emails with highly
  | costomized layouts.
 * 
 * @package     CodeIgniter
 * @author      THILAN PATHIRAGE
 * @license     http://opensource.org/licenses/MIT MIT License
 * @version     0.1
 */
/*
 * Protected the RCE (Remote Code Execution) attacks by default in Codeigniter
 */
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Email_Library extends CI_Controller {

    /**
      | -------------------------------------------------------------------------
      | Const Variables that needed for further configuration
      | -------------------------------------------------------------------------
     */
    /* The mail sending protocol.
     *      Accepted Values:
     *          mail, sendmail, or smtp
     * 
     *      Default Value:
     *          sendmail
     * 
     *      WARNING:
     *          if its smtp, you must update the other smtp variables
     */
    const EMAIL_PROTOCOL = "sendmail";


    /* The server path to Sendmail.
     *      Default Value:
     *          /usr/sbin/sendmail
     * 
     *      WARNING:
     *          Do not Empty this variable
     */
    const EMAIL_MAILPATH = "/usr/sbin/sendmail";


    /* Character set of the Email.
     * 
     *      Accepted Values:
     *          utf-8, iso-8859-1,etc.
     * 
     *      Default Value:
     *          iso-8859-1
     * 
     *      WARNING:
     *          Do not Empty this variable
     */
    const EMAIL_CHARSET = "iso-8859-1";


    /* Character set of the Email.
     * 
     *      Accepted Values:
     *          utf-8, iso-8859-1,etc.
     * 
     *      Default Value:
     *          iso-8859-1
     * 
     *      WARNING:
     *          Do not Empty this variable
     */
    const EMAIL_WORDWRAP = TRUE;


    /* SMTP Server Address.
     * 
     *      WARNING:
     *          If the Protocol is set to smtp, this variable must not be empty
     */
    const EMAIL_SMTP_HOST = "";


    /* SMTP Server Port.
     * 
     *      WARNING:
     *          If the Protocol is set to smtp, this variable must not be empty
     */
    const EMAIL_SMTP_PORT = "";


    /* SMTP Server User Name.
     * 
     *      WARNING:
     *          If the Protocol is set to smtp, this variable must not be empty
     */
    const EMAIL_SMTP_USER = "";


    /* SMTP Server User Password.
     * 
     *      WARNING:
     *          If the Protocol is set to smtp, this variable must not be empty
     */
    const EMAIL_SMTP_PASSWORD = "";


    /* Type of mail. If you send HTML email you must send it as a complete web page. Make sure you don’t have any relative links or relative image paths otherwise they will not work.
     * 
     *      Accepted Values:
     *          text or html.
     * 
     *      Default Value:
     *          html
     * 
     *      WARNING:
     *          Do not Empty this variable
     */
    const EMAIL_CONTENT_TYPE = "html";


    /* The Logo that needs to be in all Email Templates.
     * 
     *      Default Value:
     *          https://via.placeholder.com/350x150
     * 
     *      WARNING:
     *          Do not Empty this variable
     */
    const EMAIL_TEMPLATES_LOGO = "https://via.placeholder.com/350x150";


    /* The Complimentory close that needs to be in on all Email Templates.
     * 
     */
    const EMAIL_COMPLIMENTORY_CLOSE = "";

    /*
     * Constructor initialisation
     * 
     * Codeigniter email library needs to be loaded before calling all the methods
     * or you can enable email library on autoload class in config folder
     */

    public function __construct() {
        parent::__construct();
        $this->load->library('email');
    }

    /**
      | -------------------------------------------------------------------------
      | Configuration Functions
      |
      | WARRNING: Do not change this methods unless you know what you doing.
      | -------------------------------------------------------------------------
     */

    /**
     * Set Main Configuration Function
     * 
     * This will set the Mail Server Configurations and other advance configurations.
     * You can always customize this when ever you need.
     * 
     * 
     * @return Boolean Gives the result of the initialising process 
     */
    private function setMainConfiguration() {
        // set the const values to codeigniter email configs
        $config['protocol'] = self::EMAIL_PROTOCOL;
        $config['mailpath'] = self::EMAIL_MAILPATH;
        $config['charset'] = self::EMAIL_CHARSET;
        $config['wordwrap'] = self::EMAIL_WORDWRAP;
        $config['mailtype'] = self::EMAIL_CONTENT_TYPE;

        // check whether the email protocol is smtp
        if (self::EMAIL_PROTOCOL == "smtp") {
            // if the email protocol changed to smtp, setting up the smtp configs
            $config['smtp_host'] = self::EMAIL_SMTP_HOST;
            $config['smtp_port'] = self::EMAIL_SMTP_PORT;
            $config['smtp_user'] = self::EMAIL_SMTP_USER;
            $config['smtp_pass'] = self::EMAIL_SMTP_PASSWORD;
        }

        // calling the Codeigniter email initializing function to initiate the configs
        $this->email->initialize($config);
        return TRUE;
    }

    /**
     * Senders Config Function
     * 
     * This will set the sender and receiver values to the Codeigniter built in functions.
     * You can always customize this when ever you need.
     * 
     * @param Array $sendConfig All the details of the sender details
     * 
     *      Prototype: 
     * 
     *          $sendConfig=array(
     *              "sender_email"=>"jhonedoe@abc.com",
     *              "sender_name"=>"Jhone Doe", 
     *              "recever_email"=>"doejhone@abc.com",
     *              "cc_email"=>"abc@abc.com"
     *          );
     * 
     * @return Boolean this will only return FALSE during the validation. 
     */
    private function sendersConfig($sendConfig) {
        // check if the array is empty
        $result = TRUE;
        if (count($sendConfig) > 0) {

            // check if the array has the reqired element
            if (array_key_exists("sender_email", $sendConfig) & array_key_exists("sender_name", $sendConfig)) {

                // check if the element is empty
                if (isset($sendConfig["sender_email"]) & isset($sendConfig["sender_name"])) {

                    // call the Codeigniter buit in function to set the velues
                    $this->email->from($sendConfig["sender_email"], $sendConfig["sender_name"]);
                } else {
                    $result = FALSE;
                }
            } else {
                $result = FALSE;
            }

            // check if the array has the reqired element
            if (array_key_exists("recever_email", $sendConfig) & array_key_exists("recever_email", $sendConfig)) {

                // check if the element is empty
                if (isset($sendConfig["recever_email"]) & isset($sendConfig["recever_email"])) {

                    // call the Codeigniter buit in function to set the velues
                    $this->email->to($sendConfig["recever_email"]);
                } else {
                    $result = FALSE;
                }
            } else {
                $result = FALSE;
            }

            // check if the array has the reqired element
            if (array_key_exists("cc_email", $sendConfig)) {

                // check if the element is empty
                if (isset($sendConfig["cc_email"])) {

                    // call the Codeigniter buit in function to set the velues
                    $this->email->cc($sendConfig["cc_email"]);
                }
            } else {
                $result = FALSE;
            }
        } else {
            $result = FALSE;
        }

        return $result;
    }

    /**
      | -------------------------------------------------------------------------
      | Usable Functions
      |
      | WARRNING: Do not change this methods unless you know what you doing.
      | -------------------------------------------------------------------------
     */

    /**
     * Basic Text Email Function
     * 
     * Basic Email Function to send plain text. You can always customize this
     * when ever you need.
     * 
     * @param Array $sendConfig All the details of the sender details
     * 
     *      Prototype: 
     * 
     *          $sendConfig=array(
     *              "sender_email"=>"jhonedoe@abc.com",
     *              "sender_name"=>"Jhone Doe", 
     *              "recever_email"=>"doejhone@abc.com",
     *              "cc_email"=>"abc@abc.com"
     *          );
     *
     * 
     * @param String $subject The Subject of the Email
     * 
     * @param String $message The Plain Text Message of the Email
     * 
     * @return Boolean Gives the final result whether the email is successfully sent
     */
    protected function basicTextEmail($sendConfig, $subject, $message) {

        // initialze the main configurations
        if ($this->setMainConfiguration()) {
            // set the sendersconfig details
            if ($this->sendersConfig($sendConfig)) {
                // check this subject and message is'nt empty
                if ($subject != "" & $message != "") {
                    // set the subject to codeigniter buitl in function
                    $this->email->subject($subject);
                    // set the message to codeigniter buitl in function
                    $this->email->message($message);
                    // call the send method to send the email
                    $this->email->send();

                    return TRUE;
                } else {
                    print_r("error in content");
                    return FALSE;
                }
            } else {
                print_r("error in config");
                return FALSE;
            }
        } else {
            print_r("error in Mainconfig");
            return FALSE;
        }
    }

    /**
     * Email Templates : Alert Email
     * 
     * Basic Alert Email Template that can be use in many projects. 
     * You can always customize this when ever you need.
     * 
     * @param Array $sendConfig All the details of the sender details
     * 
     *      Prototype: 
     * 
     *          $sendConfig=array(
     *              "sender_email"=>"jhonedoe@abc.com",
     *              "sender_name"=>"Jhone Doe", 
     *              "recever_email"=>"doejhone@abc.com",
     *              "cc_email"=>"abc@abc.com"
     *          );
     *
     * 
     * @param String $subject The Subject of the Email
     * 
     * @param String $mailTitle The Title of the Email Alert
     * 
     * @param String $salutation The proper salutation that needs to include when sending the email.
     * 
     *      Example:
     *          "Dear User", "Hi User", etc.
     * 
     * @param String $emailbody The Main Email Content that you want to send
     * 
     * @param Array $alertbutton If you need to add a button to alert message.
     * 
     *       Prototype: 
     * 
     *          $alertbutton=array(
     *              "buttontitle"=>"Click here to see",
     *              "buttonlink"=>"full_link_to_the_action",
     *          );
     * 
     * @return Boolean Gives the final result whether the email is successfully sent
     */
    protected function alertEmailTemplate($sendConfig, $subject, $mailTitle, $salutation, $emailbody, $alertbutton) {
// initialze the main configurations
        if ($this->setMainConfiguration()) {
            // set the sendersconfig details
            if ($this->sendersConfig($sendConfig)) {
                // button design variable
                $button = '';
                // check if the button is exsist
                if (count($alertbutton) > 0) {
                    // check if the button attributes are exsist
                    if (array_key_exists("buttontitle", $alertbutton) & array_key_exists("buttonlink", $alertbutton)) {
                        // setting up the button design
                        $button = '<tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                                <center><a href="' . $alertbutton["buttonlink"] . '" class="btn-primary" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: uppercase; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;">' . $alertbutton["buttontitle"] . '</a></center>
                                            </td>
                                        </tr>';
                    } else {
                        return FALSE;
                    }
                } else {
                    return FALSE;
                }

// main Email content
                $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Web Mail</title>


        <style type="text/css">
            img {
                max-width: 100%;
            }
            body {
                -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em;
            }
            body {
                background-color: #f6f6f6;
            }
            @media only screen and (max-width: 640px) {
                body {
                    padding: 0 !important;
                }
                h1 {
                    font-weight: 800 !important; margin: 20px 0 5px !important;
                }
                h2 {
                    font-weight: 800 !important; margin: 20px 0 5px !important;
                }
                h3 {
                    font-weight: 800 !important; margin: 20px 0 5px !important;
                }
                h4 {
                    font-weight: 800 !important; margin: 20px 0 5px !important;
                }
                h1 {
                    font-size: 22px !important;
                }
                h2 {
                    font-size: 18px !important;
                }
                h3 {
                    font-size: 16px !important;
                }
                .container {
                    padding: 0 !important; width: 100% !important;
                }
                .content {
                    padding: 0 !important;
                }
                .content-wrap {
                    padding: 10px !important;
                }
                .invoice {
                    width: 100% !important;
                }
            }
        </style>
    </head>

    <body itemscope itemtype="http://schema.org/EmailMessage" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6">

        <table class="body-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;" bgcolor="#f6f6f6"><tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
                <td class="container" width="600" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
                    <div class="content" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;" bgcolor="#fff"><tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="alert alert-warning" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: orange; margin: 0; padding: 20px; text-transform: uppercase;" align="center" bgcolor="#FF9F00" valign="top">
                                    ' . $mailTitle . '
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <br/>
                                    <center><img src="' . self::EMAIL_TEMPLATES_LOGO . '" style="width:220px;" /></center>
                                </td>
                            </tr>

                            <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-wrap" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;" valign="top">
                                    <table width="100%" cellpadding="0" cellspacing="0" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                                ' . $salutation . ', 
                                            </td>
                                        </tr><tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                                ' . $emailbody . '
                                            </td>
                                        </tr>
                                            ' . $button . '                                        
                                        <tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
                                                ' . self::EMAIL_COMPLIMENTORY_CLOSE . '
                                            </td>
                                        </tr></table></td>
                            </tr></table><div class="footer" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
                            <table width="100%" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><tr style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"><td class="aligncenter content-block" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" align="center" valign="top">Questions? <a href="mailto:support@icoscheduler.com" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;">support@doubleupdice.com</a></td>
                                </tr></table></div></div>
                </td>
                <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
            </tr></table></body>
</html>
';
                // set the subject to codeigniter buitl in function
                $this->email->subject($subject);
                // set the message to codeigniter buitl in function
                $this->email->message($content);
                // call the send method to send the email
                $this->email->send();
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * emailValidation
     * 
     * This function will check whether the email address is valid or not
     * 
     *
     * 
     * @param String $email Email address to check
     * 
     * @return Boolean Gives the final result whether the email is valid
     */
    protected function emailValidation($email) {
        $email2 = filter_var($email, FILTER_SANITIZE_EMAIL);
        if (filter_var($email2, FILTER_VALIDATE_EMAIL)) {
            if (preg_match('/\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}\z/', $email2) && preg_match('/^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/', $email2)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
