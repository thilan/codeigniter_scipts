<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'Email_Library.php';

/**
 * 
 * 
 * This Libraray is the Main Controller that has all basic functions
 * 
 * 
 * @author Thilan Pathirage
 */
abstract class Base_controller extends Email_Library {

    // HTTP STATUS CODES
    const OK = 200;
    const CREATED = 201;
    const NOCONTENT = 204;
    const NOTMODIFIED = 304;
    const BADREQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOTFOUND = 404;
    const CONFLICT = 409;
    const SERVERERROR = 500;
    // USER LOGIN CONST
    const SESS_USER = "u_id";
    const SESS_EMAIL = "u_email";
    const NOTIFY_TYPE_MSG = 1;
    const SESS_ADMIN = "admin_id";
    // ClickSend Parameters
    const CS_AUTHENTICATION = "";
    const CS_FROM = "";
    const CS_BODY_MSG = "";

    function __construct() {
        parent::__construct();
    }

    /**
     * apiKeyGenerator
     * 
     * API Key Generator
     * 
     * @return String Api keys
     */
    protected function apiKeyGenerator() {
        $arr = array_values(unpack('N1a/n4b/N1c', openssl_random_pseudo_bytes(16)));
        $arr[2] = ($arr[2] & 0x0fff) | 0x4000;
        $arr[3] = ($arr[3] & 0x3fff) | 0x8000;
        return vsprintf('%08x-%04x-%04x-%04x-%04x%08x', $arr);
    }

    /**
     * token_keys
     * 
     * API Token Generator
     * 
     * @return Integer token keys
     */
    protected function token_keys() {
        $token = bin2hex(random_bytes(32));
        return $token;
    }

    /**
     * check_param_empty
     * 
     * Check the Parameters empty
     * 
     * @param Array $array All Input Param Values
     * @return Boolean $res Result
     */
    protected function check_param_empty($array) {
        $res = TRUE;
        foreach ($array as $key => $value) {
            $value = trim($value);
            if ($value == "" | $value == NULL) {
                $res = FALSE;
                break;
            }
        }
        return $res;
    }

    /**
     * json_encode_msgs
     * 
     * API Response when the data result success 
     * 
     * @param String $val Any Value that needs to send with the response
     * @param String $msg Success Message Title
     * @param Integer $status HTTP Status Code
     */
    protected function json_encode_msgs($val, $msg = "success", $status = self::OK) {
        $msgx['status'] = 1;
        $msgx['msg'] = $msg;
        $msgx['data'] = $val;
        return $this->output
                        ->set_status_header($status)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($msgx));
    }

    /**
     * errorMsg
     * 
     * API Response when the data result failed 
     * 
     * @param String $msg Success Message Title
     * @param Integer $status HTTP Status Code
     */
    protected function errorMsg($msg, $status = self::CONFLICT) {
        $msgx = array();
        $msgx['status'] = 0;
        $msgx['msg'] = $msg;
        $msgx['data'] = NULL;
        return $this->output
                        ->set_status_header($status)
                        ->set_content_type('application/json')
                        ->set_output(json_encode($msgx));
    }

    /**
     * upload_image_files
     * 
     * Image upload algorithm
     * 
     * @param String $path Path to be uploaded
     * @param Files $files Array of Image files
     * @return Mixed Uploaded Image Array or Boolean False
     */
    protected function upload_image_files($path, $files) {
        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'overwrite' => 1,
            'encrypt_name' => TRUE
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name'] = $files['name'][$key];
            $_FILES['images[]']['type'] = $files['type'][$key];
            $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images[]']['error'] = $files['error'][$key];
            $_FILES['images[]']['size'] = $files['size'][$key];

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $images[] = $this->upload->data()["file_name"];
            } else {
                return false;
            }
        }

        return $images;
    }

    /**
     * time_elapsed_string
     * 
     * Time calculation for lastlogin like functions
     * 
     * @param String $datetime Date and time
     * @param Boolean $full the return date String in full or not
     * @return String Date in Word format
     */
    protected function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    /**
     * check_user_loggedin
     * 
     * User Session checker
     * 
     * @return Boolean gives the user is logged in or not
     */
    protected function check_user_loggedin() {
        if ($this->session->has_userdata(self::SESS_USER) | $this->session->has_userdata(self::SESS_EMAIL)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * results_found
     * 
     * Output result Counter
     * 
     * @param Array $result Result Array Object
     */
    protected function results_found($result) {
        if (count($result) > 0 & $result != NULL & $result != "") {
            $this->json_encode_msgs($result, count($result) . self::SM_RESULT_FOUND, self::OK);
            return;
        } else {
            $this->errorMsg(self::EM_NO_RESULT_FOUND, self::CONFLICT);
            return;
        }
    }

    /**
     * ClickSend_sms_verification_send
     * 
     * ClickSend SMS Gateway API call
     * 
     * @param String $number Phone number to send the sms
     */
    protected function ClickSend_sms_verification_send($number) {
        $ch = curl_init();
        $veri = rand(1000, 9000);
        curl_setopt($ch, CURLOPT_URL, "https://rest.clicksend.com/v3/sms/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{
          \"messages\": [
            {
              \"source\": \"php\",
              \"from\": \"" . self::CS_FROM . "\",
              \"body\": \"" . self::CS_BODY_MSG . $veri . " \",
              \"to\": \"" . $number . "\"
            }
          ]
        }");
        $this->session->set_userdata('verify', $this->encryption->encrypt($veri));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Basic " . self::CS_AUTHENTICATION
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);

        $this->json_encode_msgs(NULL, "Verification Code has been Sent", self::OK);
        return;
    }

    /**
     * verify_sms_code
     * 
     * Mobile Number Verification SMS code verify algorithm
     * 
     * @param String $code SMS Verification Code
     * @return Boolean Gives the code is verified or not
     */
    protected function verify_sms_code($code) {
        if ($code == $this->encryption->decrypt($this->session->userdata('verify'))) {

            return TRUE;
        } else {

            return FALSE;
        }
    }

    /**
     * get_SystemGen_Password
     * 
     * System Generated Password algorithm
     * 
     * @return Array Gives an Array of encrypted password along with the raw password
     */
    protected function get_SystemGen_Password() {
        $password = random_string('alnum', 8);
        $encrypt = password_hash($password, PASSWORD_DEFAULT);
        return array("password" => $password, "encrypt" => $encrypt);
    }

    /**
     * Web_error_msg
     * 
     * Web Frontend Error Message
     * 
     */
    protected function Web_error_msg($msg) {
        $this->session->set_flashdata('errormsg', $msg);
    }

    /**
     * Web_success_msg
     * 
     * Web Frontend Success Message
     * 
     */
    protected function Web_success_msg($msg) {
        $this->session->set_flashdata('successmsg', $msg);
    }

    /**
     * Web_redirect_reff
     * 
     * Web Frontend Redirect URL
     * 
     */
    protected function Web_redirect_reff() {
        redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    /**
     * sendPushNotification
     * 
     * API FireBase Push Notifications
     * 
     */
    protected function sendPushNotification($to = array(), $notification = array(), $data = array()) {
        $apikey = '';
        $fields = array(
            'registration_ids' => $to,
            'priority' => 10,
            'data' => $data,
            'notification' => $notification,
        );

        $headers = array(
            'Authorization: key=' . $apikey,
            'Content-Type: application/json'
        );

        $url = 'https://fcm.googleapis.com/fcm/send';

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarily
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }

}
